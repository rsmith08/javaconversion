﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JavaConversion
{
    public partial class ChangeSizeForm : Form
    {
        Form1 form1;
        private int x, y;
        public ChangeSizeForm(Form1 form1, int x, int y)
        {
            this.form1 = form1;
            InitializeComponent();
            this.x = x;
            this.y = y;
            XBoxSize.Text = "" + x;
            YBoxSize.Text = "" + y;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChangeSizeForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //confirm click update call
            //we first need to check if it is valid number
            try
            {
                int tempx = Convert.ToInt32(XBoxSize.Text);
                //if it doesnt fail means correct value
                try
                {
                    int tempy = Convert.ToInt32(YBoxSize.Text);
                    //if they are valid continue
                    bool failed = false;
                    string error = "";
                    if (tempx < 150)
                    {
                       failed = true;
                        error = error + "X Value needs to be larger than 150 to produce a decent sized image. Please increase x. ";
                    }
                    if (tempy < 150)
                    {
                        
                        failed = true;
                        error = error + "Y Value needs to be larger than 150 to produce a decent size image. Please increase Y.";
                    }
                    if (failed)
                    {
                        MessageBox.Show(error);
                        this.Close();
                    }
                    else
                    {
                        form1.RefreshForm(tempx, tempy);
                        this.Close();
                    }
                }
                catch (Exception e2)
                {
                    MessageBox.Show("Y Value entered not valid, Value will not be changed");
                    this.Close();
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show("X Value entered not valid, Value will not be changed");
                this.Close();
            }
        }
    }
}
