﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace JavaConversion
{
    public partial class Form1 : Form
    {
        //used to resize form when loading picture of specific size
        private static int xIncrease = 43;
        private static int yIncrease = 98;
       
        

        private int MAX = 256;      // max iterations
        private const double SX = -2.025; // start value real
        private const double SY = -1.125; // start value imaginary
        private const double EX = 0.6;    // end value real
        private const double EY = 1.125;  // end value imaginary
        private static int x1, y1, xs, ys, xe, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;

        private  bool action, rectangle, finished, clicked;
        private SaveFileDialog save1 = new SaveFileDialog();
        public BackgroundWorker BgWorker = new BackgroundWorker();
        private float xy;
        private int maxPictureX = 640;
        private int maxPictureY = 480;
        private Pen p2 = new Pen(Color.Red, 1);
        private Bitmap picture;
       // private Graphics g1;
        private Cursor c1 = Cursors.Cross;
        private HSB HSBcol = new HSB();
        private Pen p3 = new Pen(Color.White, 0);
        private Graphics g2;
        private OpenFileDialog ofd = new OpenFileDialog();
        private ColorPalette cp;
        private Bitmap temp = null;
        private bool cycled = false;

        /**
         *  Constructor for Form 1
         * 
         * */
        public Form1()
        {
            
            InitializeComponent();
            this.DoubleBuffered = true;
            revertImageToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.None;

            action = false;
            rectangle = false;
            //max x 
            x1 = maxPictureX;
            //max y
            y1 = maxPictureY;
            picture = new Bitmap(x1, y1);
          
            xy = (float)x1 / (float)y1;
           
           g2 = Graphics.FromImage(picture);
            //g1 = Graphics.FromImage(picture);
            finished = true;

            initvalues();

            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            //we call mandelbrot onto bitmap 
            //asign the do work functions
            //and update
            
            BgWorker.DoWork += BgWorker_DoWork;
            BgWorker.ProgressChanged += BgWorker_ProgressChanged;
            BgWorker.RunWorkerCompleted += BgWorker_RunWorkerCompleted;
            BgWorker.WorkerReportsProgress = true;
            this.toolStripStatusLabel1.Text = "Generating Image...";
            BgWorker.RunWorkerAsync();
            //panel size change event
            save1.FileOk += SaveFileOk;
        }

        // getter and setter for itterations
        public int Itterations
        {
            get { return MAX; }
            set { MAX = value; }
        }
        //getter for x size
        public int Sizex
        {
            get { return maxPictureX; }
        }
        //getter for y size
        public int SizeY
        {
            get { return maxPictureY; }
        }
        // event method when mouse enters panel 1
        private void panel1_MouseEnter(object sender, EventArgs e)
        {
            this.Cursor = c1;
            rectangle = false;
        }
        //conversion method
        private Image ConvertImage(Bitmap b)
        {

            using (MemoryStream ms = new MemoryStream())
            {
                b.Save(ms, ImageFormat.Gif);
                ms.Position = 0;
                return Image.FromStream(ms);
            }

             
        }

        // event method when mouse leaves panel 1
        private void panel1_MouseLeave(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        // event method when form is finished resizing event does not fire for maxising 
        private void Form1_ResizeEnd(object sender, EventArgs e)
        {
            RefreshPanel1();
        }
        //event method to open xml file with image info
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //we need to get a selected one first
           
            ofd.Title = "Open XML File to generate past image";
            ofd.Filter = "XML Documents|*.xml";
            ofd.FileOk += FileOpenOk;
            ofd.ShowDialog();
        }
        //file open action
        private void FileOpenOk(object sender, CancelEventArgs e)
        {
            try
            {
                Console.WriteLine("We are here");
                //we need to deserialise
                XmlSerializer si = new XmlSerializer(typeof(SaveInfo));
                //FileStream.  
                FileStream myFileStream = new FileStream(ofd.FileName, FileMode.Open);
                // Call the Deserialize method and cast to the object type.  
                SaveInfo sBack = (SaveInfo)si.Deserialize(myFileStream);
                myFileStream.Close();
                RefreshForm(sBack.SizeX, sBack.SizeY, sBack.XZoom, sBack.YZoom, sBack.Itteration, sBack.XStart, sBack.YStart);
            }
            catch (Exception e1)
            {
                Console.Write(""+e1.Message);
                MessageBox.Show("Sorry the Selected file is currently unavaliable.\nPlease try again later.");   
            }

        }
        //refresh method for all variables
        private void RefreshForm(int x, int y, double xzoomNew, double yzoomNew, int itterations, double XStart, double YStart)
        {
            action = false;
            rectangle = false;
            //max x 
            x1 = maxPictureX = x;
            //max y
            y1 = maxPictureY = y;
            picture = new Bitmap(x1, y1);

            xy = (float)x1 / (float)y1;

            g2 = Graphics.FromImage(picture);
            //g1 = Graphics.FromImage(picture);
            finished = true;

            initvalues();
            xstart = XStart;
            ystart = YStart;
            xzoom = xzoomNew;
            yzoom = yzoomNew;
            MAX = itterations;
            this.Width = x + xIncrease;
            this.Height = y + yIncrease;
            this.toolStripStatusLabel1.Text = "Generating Image...";
            BgWorker.RunWorkerAsync();
        }
        //refresh method for size of image
        public void RefreshForm(int x, int y)
        {


            action = false;
            rectangle = false;
            //max x 
            x1 = maxPictureX = x;
            //max y
            y1 = maxPictureY = y;
            picture = new Bitmap(x1, y1);

            xy = (float)x1 / (float)y1;

            g2 = Graphics.FromImage(picture);
            //g1 = Graphics.FromImage(picture);
            finished = true;

            initvalues();

            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            //we call mandelbrot onto bitmap 
            //asign the do work functions
            //and update

            this.toolStripStatusLabel1.Text = "Generating Image...";
            BgWorker.RunWorkerAsync();
            this.Width = x + xIncrease;
            this.Height = y + yIncrease;
            //panel size change event
        }
        //refresh method just for itterations or depth
        public void RefreshForm( int itterations)
        {
            action = false;
            rectangle = false;
          
            finished = true;

            initvalues();

            MAX = itterations;
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            this.toolStripStatusLabel1.Text = "Generating Image...";
            BgWorker.RunWorkerAsync();
        }
        private void windowSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            Console.WriteLine("Window Size is X: "+ ActiveForm.Size.Width+"Y: " + ActiveForm.Size.Height);
        }
        // Button click event for changine iteration amount
        private void changeIterationAmountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ItterationForm tempForm = new ItterationForm(MAX, this);
            tempForm.Show();
        }

        private void startStopAnimatopmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cycled)
            {
                timer1.Enabled = false;
                //enable revert button if temp is not null
                
                
                
            }
            else
            {
                temp = picture;
                picture =(Bitmap) ConvertImage(picture);
                timer1.Enabled = true;
                cycled = true;
                revertImageToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.ImageAndText;
            }
        }

        private void changePictureSizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ChangeSizeForm csf = new ChangeSizeForm(this, maxPictureX, maxPictureY);
            csf.Show();
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpForm hf = new HelpForm();
            hf.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {   
            ColorPalette cp = picture.Palette;
            //Console.WriteLine(picture.Palette.ToString());
            
            Color first = cp.Entries[0];
            for (int i = 0; i < (cp.Entries.Length - 1); i++)
            {
                cp.Entries[i] = cp.Entries[i + 1];
            }
            cp.Entries[(cp.Entries.Length - 1)] = first;
            picture.Palette = cp;


            panel1.Invalidate();

        }

        //reverts when 
        private void revertImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (cycled)
            {
                picture = temp;
                temp = null;
                revertImageToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.None;
                //if timer is still running stop timer
                if (timer1.Enabled)
                {
                    timer1.Enabled = false;
                }
                cycled = false;
                panel1.Invalidate();
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            revertImageToolStripMenuItem.DisplayStyle = ToolStripItemDisplayStyle.None;
            if (cycled)
            {
                //we need to stop timer
                timer1.Enabled = false;
                cycled = false;
            }

            action = false;
            rectangle = false;
            //max x 
            x1 = maxPictureX;
            //max y
            y1 = maxPictureY;
            picture = new Bitmap(x1, y1);

            xy = (float)x1 / (float)y1;

            g2 = Graphics.FromImage(picture);
            //g1 = Graphics.FromImage(picture);
            finished = true;

            initvalues();

            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            this.toolStripStatusLabel1.Text = "Generating Image...";
            BgWorker.RunWorkerAsync();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult d1 = MessageBox.Show("Are you sure you want to exit", "Are you sure?",MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
            if (d1 == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        //when save item is clicked
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //we need to open file saving thing
            save1.Filter = "Image files (*.jpg, *.jpeg, *.bmp, *.png, *.gif)| *.jpg; *.jpeg; *.gif; *.png; *.bmp";
            save1.Title = "Save your Mandelbrot as: ";
            save1.ShowDialog();

        }
        //when the save file dialog is clicked ok
        private void SaveFileOk(object sender, CancelEventArgs e)
        {
            String fileName = save1.FileName;
            //we save the bitmap image and we save an xml with image size and x zoom and y zoom
            String[] FileNameSplit = fileName.Split('.');

            if (FileNameSplit.Length >= 2)
            {
                if (String.Equals(FileNameSplit.Last().ToLower(), "gif"))
                {
                    picture.Save(fileName, ImageFormat.Gif);
                    
                }
                else if (String.Equals(FileNameSplit.Last().ToLower(), "jpg"))
                {
                    picture.Save(fileName, ImageFormat.Jpeg);
                    
                }
                else if (String.Equals(FileNameSplit.Last().ToLower(), "jpeg"))
                {
                    picture.Save(fileName, ImageFormat.Jpeg);

                }
                else if (String.Equals(FileNameSplit.Last().ToLower(), "bmp"))
                {
                    picture.Save(fileName, ImageFormat.Bmp);
                   
                }
                else if (String.Equals(FileNameSplit.Last().ToLower(), "png"))
                {
                    picture.Save(fileName, ImageFormat.Png);

                }
                else
                {
                    picture.Save(fileName+".jpeg", ImageFormat.Jpeg);

                }

            }
            else
            {
                picture.Save(fileName + ".jpeg", ImageFormat.Jpeg);

            }
            
            SaveInfo si = new SaveInfo(xzoom,yzoom, maxPictureX, maxPictureY, MAX, xstart, ystart);
            
            XmlSerializer mySerializer = new XmlSerializer(typeof(SaveInfo));

            StreamWriter sw = new StreamWriter(FileNameSplit[0]+".xml");
            mySerializer.Serialize(sw, si);
            sw.Close();
        
            
            // To write to a file, create a StreamWriter object.  
            
            /**
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(FileNameSplit[0]+".bin", FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, si);
            stream.Close();
            **/

        }




        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (action)
            {
                if (clicked && !cycled)
                {
                    xe = e.Location.X;
                    ye = e.Location.Y;
                    rectangle = true;
                    panel1.Invalidate();
                    //panel1.Refresh();
                }
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            //Console.WriteLine("Mouse Down..");
            if (!cycled) { 
                clicked = true;
                rectangle = true;
                if (action)
                {
               
                
                    xs = e.X;
                    ys = e.Y;
                }
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            int z, w;
            //Console.WriteLine("Mouse Up..");
            clicked = false;
            if (action && !cycled)
            {
                xe = e.X;
                ye = e.Y;
                if (xs > xe)
                {
                    z = xs;
                    xs = xe;
                    xe = z;
                }
                if (ys > ye)
                {
                    z = ys;
                    ys = ye;
                    ye = z;
                }
                w = (xe - xs);
                z = (ye - ys);
                if ((w < 2) && (z < 2))
                    initvalues();
                else
                {
                    if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                    else xe = (int)((float)xs + (float)z * xy);
                    xende = xstart + xzoom * (double)xe;
                    yende = ystart + yzoom * (double)ye;
                    xstart += xzoom * (double)xs;
                    ystart += yzoom * (double)ys;
                }
                xzoom = (xende - xstart) / (double)x1;
                yzoom = (yende - ystart) / (double)y1;
                rectangle = false;
                panel1.Refresh();
                this.toolStripStatusLabel1.Text = "Generating Image...";
                BgWorker.RunWorkerAsync();
                
                
            }
        }
        
       
        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            /*if (x1 != panel1.Size.Width && y1 != panel1.Size.Height) {
                //max x 
                x1 = panel1.Size.Width;
                //max y
                y1 = panel1.Size.Height;
                
                if(!BgWorker.IsBusy)
                    BgWorker.RunWorkerAsync();
            }**/
            
            e.Graphics.DrawImageUnscaled(picture, 0, 0);
            if (rectangle)
            {
               
                if (xs < xe)
                {
                    if (ys < ye) e.Graphics.DrawRectangle(p3,xs, ys, (xe - xs), (ye - ys));
                    else e.Graphics.DrawRectangle(p3, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) e.Graphics.DrawRectangle(p3, xe, ys, (xs - xe), (ye - ys));
                    else e.Graphics.DrawRectangle(p3, xe, ye, (xs - xe), (ys - ye));
                }
            }
            //Pen myPen = new Pen(Color.Red, 2); //create a pen object



            //g1.DrawLine(myPen, 0, 0, maxPictureX, maxPictureY); //use the DrawLine method

        }
        /**
         *  Background worker to recalculate the mandelbrot so the GUI doesnt freeze or lag
         * 
         * */
        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            //toolStripStatusLabel1.Text = "Starting generation...";
            //MessageBox.Show("Starting generation..");
            
            mandelbrot();
        }

        private void BgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //Console.WriteLine(e.ProgressPercentage);
            toolStripProgressBar1.Value = e.ProgressPercentage;
        }

        private void BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //MessageBox.Show("Done");
            //toolStripStatusLabel1.Text = "Complete..";
            //once its complete get the colour pallet
           
            
            this.toolStripStatusLabel1.Text = "Generating Image Complete";
            panel1.Refresh();
        }

        private void initvalues() // reset start values
        {
            clicked = false;
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        private void RefreshPanel1()
        {
            /**maxPictureX = panel1.Size.Width;
            maxPictureY = panel1.Size.Height;
            //max x 
            x1 = maxPictureX;
            //max y
            y1 = maxPictureY;
            picture = new Bitmap(x1, y1);

            xy = (float)x1 / (float)y1;

            g2 = Graphics.FromImage(picture);
            g1 = Graphics.FromImage(picture);
            finished = true;

            initvalues();

            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            BgWorker.RunWorkerAsync();**/
        }


        

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
           // Console.WriteLine("H value:" + (float)j / (float)MAX);
            return (float)j / (float)MAX;
        }
        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;
            
            action = false;
            
            for (x = 0; x < x1; x = x +2) {
                for (y = 0; y < y1; y++)
                {
                   // Console.WriteLine("Xfinal is:" +( xstart + xzoom * (double)x)+"Y final is: "+ (ystart + yzoom * (double)y));
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                   // Console.WriteLine("H is: " + h);
                    if (h != alt)
                    {
                        //Console.WriteLine("colour would be changed");
                        b = 1.0f - h * h; // brightnes
                                          ///djm added
                                          ///HSBcol.fromHSB(h,0.8f,b); //convert hsb to rgb then make a Java Color
                                          ///Color col = new Color(0,HSBcol.rChan,HSBcol.gChan,HSBcol.bChan);
                                          ///g1.setColor(col);
                        //djm end
                        //djm added to convert to RGB from HSB

                        // g1.setColor(Color.getHSBColor(h, 0.8f, b));
                        //djm test
                        //Color col = Color.getHSBColor(h, 0.8f, b);
                        //int red = col.getRed();
                        //int green = col.getGreen();
                        //int blue = col.getBlue();
                        Color c = HSBcol.FromHSB(h*255, 0.8f*255,b*255);
                        p2 = new Pen(c, 1);
                        //Console.WriteLine("New Colour"+ c.ToString());
                        //djm 
                        alt = h;
                    }
                    g2.DrawLine(p2, x, y, x + 1, y);
                    //Console.WriteLine("We have drawn another line");
                }
                if ((x+2)%10 == 0)
                {
                    //Console.WriteLine("X: "+(x+2)+" X1: "+x1);
               // Console.WriteLine(""+((float)x/x1));
                float temp = (float)(x+2) / (float)x1;
                temp = temp * 100;
                int percent = (int)temp;

               // Console.WriteLine(percent);
                BgWorker.ReportProgress(percent);
                }
                
                    
                }
            
            action = true;
        }

    }


    public class HSB
    {
        private float rChan, bChan, gChan;
        private float RCHAN
        {
            get { return rChan; }
        }
           private float GCHAN
        {
        get { return gChan; }
        }
        private float BCHAN
        {
            get { return bChan; }
        }
        //could use expresion body so public HSB() => rChan = gChan = bChan = 0;
        //not sure why yet
        public HSB()
        {
            rChan = gChan = bChan = 0;
        }

        public int getRChan() {
            return (int)this.rChan;
        }
        public int getgChan()
        {
            return (int)this.gChan;
        }
        public int getbChan()
        {
            return (int)this.bChan;
        }
        public Color FromHSB(float h, float s, float b)
        {
            float red = b;
            float green = b;
            float blue = b;
            int alpha = 0xFF;

            if (s != 0)
            {
                float max = b;
                float dif = b * s / 255f;
                float min = b - dif;

                float h2 = h * 360f / 255f;

                if (h2 < 60f)
                {
                    red = max;
                    green = h2 * dif / 60f + min;
                    blue = min;
                }
                else if (h2 < 120f)
                {
                    red = -(h2 - 120f) * dif / 60f + min;
                    green = max;
                    blue = min;
                }
                else if (h2 < 180f)
                {
                    red = min;
                    green = max;
                    blue = (h2 - 120f) * dif / 60f + min;
                }
                else if (h2 < 240f)
                {
                    red = min;
                    green = -(h2 - 240f) * dif / 60f + min;
                    blue = max;
                }
                else if (h2 < 300f)
                {
                    red = (h2 - 240f) * dif / 60f + min;
                    green = min;
                    blue = max;
                }
                else if (h2 <= 360f)
                {
                    red = max;
                    green = min;
                    blue = -(h2 - 360f) * dif / 60 + min;
                }
                else
                {
                    red = 0;
                    green = 0;
                    blue = 0;
                }
            }
            //rChan = (float)255 * red;
            //gChan = (float)255 * green;
            //bChan = (float)255 * blue;
            rChan = (float) Math.Round(Math.Min(Math.Max(red, 0f), 255));
            gChan = (float) Math.Round(Math.Min(Math.Max(green, 0), 255));
            bChan = (float)Math.Round(Math.Min(Math.Max(blue, 0), 255));
            return Color.FromArgb(alpha, (int)rChan, (int)gChan, (int)bChan );
        }

    }
}

