﻿namespace JavaConversion
{
    partial class ChangeSizeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.YBoxSize = new System.Windows.Forms.TextBox();
            this.XBoxSize = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(104, 141);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Confirm";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "X Size";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Y Size";
            // 
            // YBoxSize
            // 
            this.YBoxSize.Location = new System.Drawing.Point(77, 97);
            this.YBoxSize.Name = "YBoxSize";
            this.YBoxSize.Size = new System.Drawing.Size(183, 20);
            this.YBoxSize.TabIndex = 4;
            this.YBoxSize.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChangeSizeForm_KeyDown);
            // 
            // XBoxSize
            // 
            this.XBoxSize.Location = new System.Drawing.Point(77, 61);
            this.XBoxSize.Name = "XBoxSize";
            this.XBoxSize.Size = new System.Drawing.Size(183, 20);
            this.XBoxSize.TabIndex = 5;
            this.XBoxSize.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChangeSizeForm_KeyDown);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(185, 141);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Please enter in the new size of the image";
            // 
            // ChangeSizeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(272, 176);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.XBoxSize);
            this.Controls.Add(this.YBoxSize);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "ChangeSizeForm";
            this.Text = "Change Picture Resolution";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChangeSizeForm_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox YBoxSize;
        private System.Windows.Forms.TextBox XBoxSize;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
    }
}