﻿using System;
using System.Xml.Serialization;

namespace JavaConversion
{
    [Serializable]
    public class SaveInfo
    {
        public double XZoom, YZoom, XStart, YStart;
        public int SizeX, SizeY, Itteration;


        public SaveInfo()
        {
            XZoom = YZoom = XStart = YStart = 0.0f;
            SizeX = SizeY = Itteration = 0;
        }

        public SaveInfo(double XZoom, double YZoom, int SizeX, int SizeY, int Itteration, double XStart, double YStart)
        {
            this.SizeY = SizeY;
            this.SizeX = SizeX;
            this.XZoom = XZoom;
            this.YZoom = YZoom;
            this.Itteration = Itteration;
            this.XStart = XStart;
            this.YStart = YStart;

        }
    }
}
