﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JavaConversion
{
    public partial class ItterationForm : Form
    {
        private int Itteration;
        Form1 form1;
        public ItterationForm(int Itteration, Form1 f)
        {
            InitializeComponent();
            this.Itteration = Itteration;
            textBox2.Text = ""+Itteration;
            form1 = f;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //we first need to check if it is valid number
            try
            {
                int temp = Convert.ToInt32(textBox2.Text);
                //if it doesnt fail means correct value
                if (temp > 30)
                {
                    form1.RefreshForm(temp);
                    
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Number entered not valid, Value will not be changed");
                    this.Close();
                }
            }
            catch (Exception e1)
            {
                MessageBox.Show("Number entered not valid, Value will not be changed");
                this.Close();
            }
        }

        private void ItterationForm_Load_1(object sender, EventArgs e)
        {

        }

        private void ItterationForm_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void ItterationForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }
    }
}
